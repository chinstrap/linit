prefix      ?= /usr
exec_prefix ?= $(prefix)
bindir      ?= $(exec_prefix)/bin
libexecdir  ?= $(exec_prefix)/libexec
sysconfdir  ?= /etc

CFLAGS += -std=c99 -Wall -Wextra -Werror -pedantic

.PHONY: all
all: linit lreboot

linit: config.h

config.h: config.h.in
	sed -e 's|@sysconfdir@|$(sysconfdir)|g' \
	    -e 's|@libexecdir@|$(libexecdir)|g' \
	    $^ > $@

.PHONY: clean
clean:
	rm -f -- config.h linit lreboot
