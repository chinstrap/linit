/* SPDX-License-Identifier: 0BSD */

#define _XOPEN_SOURCE 700
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "config.h"

#define LINIT "/linit"
#define LINIT_BOOT    LINIT "/boot"
#define LINIT_SIGINT  LINIT "/sigint"
#define LINIT_SIGPWR  LINIT "/sigpwr"

extern char **environ;

/**
 * linit_err:
 * @err: negative errno-style error code
 *
 * Attempts to publish the error to stderr and /dev/kmsg.
 **/
void
linit_err(int ret)
{
	errno = -ret;
	perror("linit");
	int kfd = open("/dev/kmsg", O_WRONLY | O_NOCTTY);
	if (kfd < 0)
		return;
	dprintf(kfd, "<2>linit: %s\n", strerror_l(-ret, NULL));
	close(kfd);
}

/**
 * linit_spawn:
 * @path: absolute path to binary to spawn
 *
 * Spawns a child process with default signal handlers, an empty signal
 * mask, and a new process group.
 *
 * Returns: 1 on successful spawn, 0 if @path does not exist, and
 *          -errno on other error conditions.
 **/
int
linit_spawn(char * restrict path)
{
	int ret;
	sigset_t set_mask, set_def;
	posix_spawnattr_t attr;
	char *argv[2] = { path, NULL };
	short spawnflags =
		POSIX_SPAWN_SETSIGMASK
		| POSIX_SPAWN_SETSIGDEF
		| POSIX_SPAWN_SETPGROUP;

	if (sigemptyset(&set_mask) != 0
	    || sigemptyset(&set_def) != 0
	    || sigaddset(&set_def, SIGINT) != 0
	    || sigaddset(&set_def, SIGPWR) != 0
	    || sigaddset(&set_def, SIGCHLD) != 0)
		return -errno;

	if ((ret = posix_spawnattr_init(&attr)) != 0)
		return -ret;

	if ((ret = posix_spawnattr_setflags(&attr, spawnflags)) != 0
	    || (ret = posix_spawnattr_setsigmask(&attr, &set_mask)) != 0
	    || (ret = posix_spawnattr_setsigdefault(&attr, &set_def)) != 0) {
		posix_spawnattr_destroy(&attr);
		return -ret;
	}

	ret = posix_spawn(NULL, path, NULL, &attr, argv, environ);

	posix_spawnattr_destroy(&attr);

	if (ret == ENOENT)
		return 0;
	else if (ret != 0)
		return -ret;

	return 1;
}

void
linit_handler(int signo)
{
	if (signo == SIGINT && linit_spawn(SYSCONFDIR LINIT_SIGINT) == 0)
		linit_spawn(LIBEXECDIR LINIT_SIGINT);
	else if (signo == SIGPWR && linit_spawn(SYSCONFDIR LINIT_SIGPWR) == 0)
		linit_spawn(LIBEXECDIR LINIT_SIGPWR);
}

/**
 * linit_signals:
 *
 * Mask all signals apart from SIGINT, SIGPWR, and SIGCHLD.
 * Set up a handler for the first two and ignore the latter.
 *
 * Returns: 0 on success, -errno on failure.
 */
int
linit_signals(void)
{
	sigset_t set;
	return (
		sigfillset(&set) == 0
		&& sigprocmask(SIG_BLOCK, &set, NULL) == 0
		&& sigset(SIGCHLD, SIG_IGN) != SIG_ERR
		&& sigset(SIGINT, linit_handler) != SIG_ERR
		&& sigset(SIGPWR, linit_handler) != SIG_ERR
	) ? 0 : -errno;
}

int
main(void)
{
	if (getpid() != 1)
		return EXIT_FAILURE;

	int ret = linit_signals();

	/* We ignore SIGCHLD above, but the initramfs may have left zombies */
	for (;;)
		if (waitpid(-1, NULL, WNOHANG) <= 0)
			break;

	if (ret == 0 && (ret = linit_spawn(SYSCONFDIR LINIT_BOOT)) == 0)
		ret = linit_spawn(LIBEXECDIR LINIT_BOOT);

	if (ret < 0)
		linit_err(ret);

	for (;;)
		pause();
}
