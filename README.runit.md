# Replacing runit-init with linit

Runit is an init system and supervision suite in the style of daemontools.
This guide will help you replace the init system component with linit.

This guide assumes that you have already installed the lreboot binary into
your path, and have installed the linit binary *somewhere*.

If you install the `linit` package on Void Linux, these two binaries should be
located in `/usr/bin`.

## Work backwards

It is advised to work in reverse when replacing a running init system.

First we create a script that can independently
[reboot(2)](https://man.voidlinux.org/man2/reboot.2)
the computer (safely) without talking to the running init system.

We will leverage the existing stage 3 of runit, then call `lreboot`.

```sh
# cat > /usr/local/bin/linit-teardown <<"EOF"
#!/bin/sh

set +e
set -uo

# This script can be called as 'linit-teardown' or as a linit hook point.
# If called as a linit hook point, the basename will be 'sigpwr' or 'sigint'.
#
# Map
# 	'sigpwr' to 'linit-teardown power_off', and
# 	'sigint' to 'linit-teardown autoboot'.
#
# Other basenames (reboot, poweroff, halt, shutdown, telinit) are not supported.

CMD="$(basename "${0}")"

case "${CMD}" in
	sigint)
		ARG=autoboot
		;;
	sigpwr)
		ARG=power_off
		;;
	linit-teardown)
		ARG="${1}"
		;;
	*)
		printf 'Unimplemented basename' >&2
		exit 2
		;;
esac

# Filter the argument to supported operations,
# and let runit stage 3 know if a reboot (autoboot) is planned.

case "${ARG}" in
	autoboot)
		touch /etc/runit/reboot
		;;
	power_off|halt_system)
		;;
	kexec)
		printf 'Command unimplemented\n' >&2
		exit 2
		;;
	sw_suspend|enable_cad|disable_cad)
		printf 'Command nonsensical\n' >&2
		exit 2
		;;
	*)
		printf 'Command invalid\n' >&2
		exit 2
		;;
esac

. /etc/runit/3

exec lreboot "${ARG}"
EOF
# chmod +x /usr/local/bin/linit-teardown
```

Try it out:

```
# setsid linit-teardown autoboot &
```

Did it reboot your system? Were runsv supervised services stopped appropriately?

If not, file a
[bug report](https://gitlab.com/chinstrap/linit/issues/new).

If so, hook it up to linit's SIGINT (control-alt-delete) and SIGPWR handlers:

```sh
# ln -s /usr/local/bin/linit-teardown /etc/linit/sigint 
# ln -s /usr/local/bin/linit-teardown /etc/linit/sigpwr
```

## Stages less than three

Unlike runit-init, linit does not distinguish between any stages or boot-time
sequences.
It runs a single hook at boot.

Let's write that hook:

```sh
# cat /etc/linit/boot <<"EOF"
#!/bin/sh
/etc/runit/1
lreboot disable_cad
exec /etc/runit/2
EOF
# chmod +x /etc/linit/boot
```

> Exercise for the reader: use runsv to supervise runit stage 2.

## Changing hands

The next step in the switch consists of pointing the kernel at linit.

This can be done through the kernel command line, or by linking /sbin/init to
wherever you installed linit.

Call `setsid linit-teardown autoboot &` again and you should boot using linit
and the runit supervision suite.

As a reminder,
[file a bug report](https://gitlab.com/chinstrap/linit/issues/new)
if you encounter any issues.
