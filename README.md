# linit

`linit` is a strictly functional init system for Linux.

It exposes the unique semantics of PID 1 in a simple and accessible way.

## Compilation

`linit` is written in C, and can be built with GNU Make:

    $ make

Two executables will be created, `linit` and `lreboot`.

## Installation

    $ install -DZ -t /usr/local/bin linit lreboot

## Configuration

`linit` executes hooks asynchronously in response to the following events:

* `sigint`: control-alt-delete key press or container runtime requested reboot.
* `sigpwr`: power event or container runtime requested shutdown.
* `boot`: the system was booted.

`linit` searches the following directories when executing the hooks.
If a hook is found, `linit` will stop searching:

* `$(sysconfdir)/linit`
* `$(libexecdir)/linit`

Example hooks can be found in the `example-hooks` directory.
These examples offer basic boot-to-shell functionality, but more complicated
hooks are described in the `README.runit.md` document.
The runit hooks are more complicated, yet offer integration with the excellent
`runit` service supervision suite.

## Usage

### linit

For a single boot, add the following to the kernel command line:

    init=/usr/local/bin/linit

To persist across reboots, add the above to your bootloader configuration, or
link `linit` to `/sbin/init`:

    # ln -s /usr/local/bin/linit /sbin/init

### lreboot

`lreboot` accepts one argument, an action to pass to the C library call
[reboot(2)](http://manpages.ubuntu.com/manpages/bionic/man2/reboot.2.html):

* `disable_cad`: equivalent to `ctrlaltdel soft`
* `enable_cad`: equivalent to `ctrlaltdel hard`
* `halt_system`: equivalent to `halt -f`
* `power_off`: equivalent to `poweroff -f`
* `autoboot`: equivalent to `reboot -f`
* `kexec`: unimplemented
* `sw_suspend`: unimplemented

See the manpage for the library call for more information on these actions.
For the halt, power_off, and autoboot actions, `lreboot` will call
[sync(2)](http://manpages.ubuntu.com/manpages/bionic/en/man2/sync.2.html)
first.

## See also

[void-runit](https://github.com/void-linux/void-runit):
runit init scripts for Void

[startup](https://gitlab.com/chinstrap/startup):
event driven task and service manager
