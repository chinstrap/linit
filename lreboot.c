/* SPDX-License-Identifier: 0BSD */

#define _XOPEN_SOURCE 500
#include <signal.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <unistd.h>
#include <sys/reboot.h>
#include <stdlib.h>
#include <stdio.h>

int
main(int argc, char **argv)
{
	int cmd;

	if (argc != 2)
		return 2;

	switch (argv[1][0]) {
	case 'a':
		sync();
		cmd = RB_AUTOBOOT;
		break;
	case 'd':
		cmd = RB_DISABLE_CAD;
		break;
	case 'e':
		cmd = RB_ENABLE_CAD;
		break;
	case 'h':
		sync();
		cmd = RB_HALT_SYSTEM;
		break;
	case 'k':
		cmd = RB_KEXEC;
		break;
	case 'p':
		sync();
		cmd = RB_POWER_OFF;
		break;
	case 's':
		cmd = RB_SW_SUSPEND;
		break;
	default:
		return 2;
	}

	if (reboot(cmd) == 0)
		return EXIT_SUCCESS;

	perror("reboot");
	return EXIT_FAILURE;
}
